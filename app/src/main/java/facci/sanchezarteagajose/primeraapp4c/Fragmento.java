package facci.sanchezarteagajose.primeraapp4c;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Fragmento extends AppCompatActivity implements View.OnClickListener, FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener{

    Button botonFranUno, botonFranDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmento);

        botonFranUno = (Button) findViewById(R.id.btnFragUno);
        botonFranDos = (Button)findViewById(R.id.btnFragDos);

        botonFranUno.setOnClickListener(this);
        botonFranDos.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnFragUno:
                FragUno frgmentoUno = new FragUno();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,frgmentoUno);
                transaction.commit();
                break;
        }
        switch (v.getId()){
            case R.id.btnFragDos:
                FragDos frgmentoDos = new FragDos();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,frgmentoDos);
                transaction.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
