package facci.sanchezarteagajose.primeraapp4c;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button buttonLogin, buttonGuardar, buttonBuscar ,buttonParametro, buttonFragmento, btnautenticar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {         // se genera la navegabilidad entre la actividad principal y la actividad de login
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
            } });
        buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {         // se genera la navegabilidad entre la actividad principal y la actividad de login
                Intent intent = new Intent(MainActivity.this, Buscar.class);
                startActivity(intent);
            } });
        buttonGuardar = (Button) findViewById(R.id.buttonGuardar);
        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {         // se genera la navegabilidad entre la actividad principal y la actividad de login
                Intent intent = new Intent(MainActivity.this, Registrar.class);
                startActivity(intent);
            } });
        buttonParametro = (Button) findViewById(R.id.buttonParametro);
        buttonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {         // se genera la navegabilidad entre la actividad principal y la actividad de login
                Intent intent = new Intent(MainActivity.this, PasarParametro.class);
                startActivity(intent);
            } });
        buttonFragmento = (Button) findViewById(R.id.btnFramento);
        buttonFragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Fragmento.class);
                startActivity(intent);
            }
        });
        btnautenticar = (Button) findViewById(R.id.btnautentic);
        btnautenticar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Autenticar.class);
                startActivity(intent);
            }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item){

        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent =new Intent (MainActivity.this,Login.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent =new Intent (MainActivity.this,Registrar.class);
                startActivity(intent);
                break;
        }
        return  true;
    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }
}
