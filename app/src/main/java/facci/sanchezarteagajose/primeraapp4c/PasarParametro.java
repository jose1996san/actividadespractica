package facci.sanchezarteagajose.primeraapp4c;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasarParametro extends AppCompatActivity {

    Button buttonEnviar;
    EditText CajaDatos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);
        CajaDatos = (EditText) findViewById(R.id.TextParametro);
        buttonEnviar = (Button) findViewById(R.id.buttonEnviarParametro);
        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {         // se genera la navegabilidad entre la actividad principal y la actividad de login
                Intent intent = new Intent(PasarParametro.this, RecibirParametro.class);

                Bundle bundle = new Bundle(); // el método put fija los parámetro a enviar mediante un id
                bundle.putString("dato",CajaDatos.getText().toString());
                intent.putExtras(bundle);// método putExtras envia un objeto de tipo bundle como un sólo parámetro entre actividades
                startActivity(intent);
            } });

    }
}
