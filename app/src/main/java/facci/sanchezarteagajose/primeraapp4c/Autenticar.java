package facci.sanchezarteagajose.primeraapp4c;

import android.app.Activity;
import android.os.Bundle;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.*;

public class Autenticar extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autenticar);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(Autenticar.this);
                dialogoLogin.setContentView(R.layout.dlg_login);

                Button btnautenticar = (Button) dialogoLogin.findViewById(R.id.btnautenticar);
                final EditText Cajausuario = (EditText) dialogoLogin.findViewById(R.id.txtusu);
                final EditText Cajaclave = (EditText) dialogoLogin.findViewById(R.id.txtclav);
                btnautenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Autenticar.this,"usuario: "+ Cajausuario.getText().toString()+"  clave: "+Cajaclave.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
                Dialog dialogoRegistrar = new Dialog(Autenticar.this);
                dialogoRegistrar.setContentView(R.layout.dlg_registrar);
                Button btnregistrar =(Button)dialogoRegistrar.findViewById(R.id.btnregistrar);
                final EditText nombre=(EditText)dialogoRegistrar.findViewById(R.id.txtnombres);
                final EditText apellido=(EditText)dialogoRegistrar.findViewById(R.id.txtapellidos);

                btnregistrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Autenticar.this, "nombres: " + nombre.getText().toString() + "  apellidos: " + apellido.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                });
                dialogoRegistrar.show();
                break;

        }
        return true;
    }

}

